/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.festineuch.festineuch.artistes.rest;

import ch.festineuch.festineuch.artistes.rest.ProgrammationRessource;
import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 *
 * @author francis.heche
 */

@ApplicationPath("/api/v1")
public class FestineuchRestConfig extends Application {
    @Override
    public Set<Class<?>> getClasses(){
        HashSet resources = new HashSet<Class<?>>();
        resources.add(ProgrammationRessource.class);
        return resources;
    }
}
