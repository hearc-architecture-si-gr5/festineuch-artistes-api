/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.festineuch.festineuch.artistes.rest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author francis.heche
 */

@RequestScoped
@Path("/top")

public class ProgrammationRessource{
 
  @GET
  @Path("/{artistenom}")
  @Produces(MediaType.APPLICATION_JSON)
  public String[][] getTitres(@PathParam("artistenom") String artisteNom) throws IOException {
              URL url = null;
        
        try {
            url = new URL("http://api.deezer.com/search?q="+artisteNom);
            

        } catch (MalformedURLException ex){
            Logger.getLogger(ProgrammationRessource.class.getName()).log(Level.SEVERE, null, ex);
        }
        URLConnection connection = url.openConnection();

        String line;
        StringBuilder builder = new StringBuilder();
        BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        while ((line = reader.readLine()) != null) {
            builder.append(line);
        }

        JSONObject jsonObj = new JSONObject(builder.toString());

        JSONArray jsonTabArtistes = jsonObj.getJSONArray("data");

        Integer idArtiste = (Integer) jsonTabArtistes.getJSONObject(0).getJSONObject("artist").get("id");
        try {
            url = new URL("http://api.deezer.com/artist/"+ String.valueOf(idArtiste) +"/top");

        } catch (MalformedURLException ex) {
            Logger.getLogger(ProgrammationRessource.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        connection = url.openConnection();

        line = "";
        builder = new StringBuilder();
        reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        while ((line = reader.readLine()) != null) {
            builder.append(line);
        }

        jsonObj = new JSONObject(builder.toString());  
        jsonTabArtistes = jsonObj.getJSONArray("data");
        
        String topTitres[][] = {{null,null},{null,null},{null,null},{null,null},{null,null}};
        
        topTitres[0][0]=(String) jsonTabArtistes.getJSONObject(0).get("title");
        topTitres[0][1]=(String) jsonTabArtistes.getJSONObject(0).get("preview");
        topTitres[1][0]=(String) jsonTabArtistes.getJSONObject(1).get("title");
        topTitres[1][1]=(String) jsonTabArtistes.getJSONObject(1).get("preview");
        topTitres[2][0]=(String) jsonTabArtistes.getJSONObject(2).get("title");
        topTitres[2][1]=(String) jsonTabArtistes.getJSONObject(2).get("preview");
        topTitres[3][0]=(String) jsonTabArtistes.getJSONObject(3).get("title");
        topTitres[3][1]=(String) jsonTabArtistes.getJSONObject(3).get("preview");
        topTitres[4][0]=(String) jsonTabArtistes.getJSONObject(4).get("title");
        topTitres[4][1]=(String) jsonTabArtistes.getJSONObject(4).get("preview");
        
        return topTitres;
  }
}